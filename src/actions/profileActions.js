import * as types from './actionTypes';
import profileApi from '../api/profileApi';

export function profileBudgetsRequest() {
  return {
    type: types.PROFILE_BUDGETS_REQUEST
  };
}

export function profileBudgetsSuccess(budgets) {
  return {
    type: types.PROFILE_BUDGETS_SUCCESS,
    budgets
  };
}

export function profileBudgetsFailure() {
  return {
    type: types.PROFILE_BUDGETS_FAILURE
  };
}

export function profileBudgetsForbidden() {
  return {
    type: types.PROFILE_BUDGETS_FORBIDDEN
  };
}

export function getProfileBudgets(profileId) {
  return function(dispatch) {
    dispatch(profileBudgetsRequest());
    return profileApi.getBudgetsForProfile(profileId)
      .then(res => {
        const budgets = res.data;
        dispatch(profileBudgetsSuccess(budgets));
      }).catch(res => {
        if(res.response.status === 403) {
          dispatch(profileBudgetsForbidden());
        } else {
          dispatch(profileBudgetsFailure());
          throw res.response.data;
        }
      });
  }
}

export function selectBudget(budgetKey) {
  return {
    type: types.PROFILE_BUDGET_SELECT,
    budgetKey
  };
}

export function saveBudgetRequest() {
  return {
    type: types.SAVE_BUDGET_REQUEST
  }
}

export function saveBudgetSuccess(budget) {
  return {
    type: types.SAVE_BUDGET_SUCCESS,
    budget
  }
}

export function saveBudgetFailure() {
  return {
    type: types.SAVE_BUDGET_FAILURE
  }
}

export function saveBudget(profileId, budget) {
  return function(dispatch) {
    dispatch(saveBudgetRequest());
    return profileApi.saveBudget(profileId, budget)
      .then(() => dispatch(saveBudgetSuccess(budget)))
      .catch((res) => {
        dispatch(saveBudgetFailure(res.response.data));
        throw res.response.data
      });
  }
}

export function createNewBudgetRequest() {
  return {
    type: types.CREATE_NEW_BUDGET_REQUEST
  }
}

export function createNewBudgetSuccess() {
  return {
    type: types.CREATE_NEW_BUDGET_SUCCESS
  }
}

export function createNewBudgetFailure() {
  return {
    type: types.CREATE_NEW_BUDGET_FAILURE
  }
}

export function createNewBudget(profileId, name, date) {
  return function(dispatch) {
    dispatch(createNewBudgetRequest());
    return profileApi.createNewBudget(profileId, name, date)
      .then((budget) => {
        dispatch(createNewBudgetSuccess());
        return budget.data;
      })
      .catch((res) => {
        dispatch(createNewBudgetFailure(res.response.data));
        throw res.response.data
      })
  }
}

export function deleteBudgetRequest() {
  return {
    type: types.DELETE_BUDGET_REQUEST
  }
}

export function deleteBudgetRequestSuccess() {
  return {
    type: types.DELETE_BUDGET_REQUEST_SUCCESS
  }
}

export function deleteBudgetRequestFailure() {
  return {
    type: types.DELETE_BUDGET_REQUEST_FAILURE
  }
}

export function deleteBudget(profileId, budgetKey) {
  return function(dispatch) {
    dispatch(deleteBudgetRequest());
    return profileApi.deleteBudget(profileId, budgetKey)
      .then(() => dispatch(deleteBudgetRequestSuccess()))
      .catch((res) => {
        dispatch(deleteBudgetRequestFailure(res.response.data));
        throw res.response.data
      })
  }
}

