import * as types from './actionTypes';
import authApi from '../api/authApi';

export function requestLogin(credentials) {
  return {
    type: types.LOGIN_REQUEST,
    isFetching: true,
    isAuthenticated: false,
    credentials
  };
}

export function loginSuccess(user) {
  return {
    type: types.LOGIN_SUCCESS,
    isFetching: false,
    isAuthenticated: true,
    userid: user.userid
  };
}

export function loginFailure(message) {
  return {
    type: types.LOGIN_FAILURE,
    isFetching: false,
    isAuthenticated: false,
    message
  };
}

export function loginUser(credentials) {
  return function(dispatch) {
    dispatch(requestLogin(credentials));
    const res = authApi.loginUser(credentials);
    return res.then(response => {
      const user = response.data;
      localStorage.setItem('id_token', user.id_token);
      dispatch(loginSuccess(user));
      return user.userid;
    }).catch(err => {
      dispatch(loginFailure(err.response.data));
      throw err.response.data;
    })
  };
}

function requestLogout() {
  return {
    type: types.LOGOUT_REQUEST,
    isFetching: true
  };
}

function receiveLogout() {
  return {
    type: types.LOGOUT_SUCCESS,
    isFetching: false,
    isAuthenticated: false
  };
}

export function logoutUser() {
  return function(dispatch) {
    dispatch(requestLogout());
    localStorage.removeItem('id_token');
    dispatch(receiveLogout());
  };
}
