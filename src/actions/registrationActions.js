import * as types from './actionTypes';
import authApi from '../api/authApi';

export function requestRegistration(registration) {
  return {
    type: types.REGISTRATION_REQUEST,
    registration,
    isRegistering: true
  };
}

export function registrationSuccess(registration, userid) {
  return {
    type: types.REGISTRATION_SUCCESS,
    isRegistering: false,
    registration,
    userid
  };
}

export function registrationFailure() {
  return {
    type: types.REGISTRATION_FAILURE,
    isRegistering: false
  };
}

export function register(registration) {
  return function(dispatch) {
    dispatch(requestRegistration(registration));
    return authApi.registerUser(registration)
      .then(response => {
        const user = response.data;
        localStorage.setItem('id_token', user.id_token);
        localStorage.setItem('access_token', user.access_token);
        dispatch(registrationSuccess(registration, user.userid));
        return user.userid;
      }).catch(err => {
        dispatch(registrationFailure());
        throw err.response.data;
      });
  }
}


