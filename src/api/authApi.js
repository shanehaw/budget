import axios from 'axios'

class AuthApi {
  static loginUser(credentials) {
    let userToLogin = Object.assign({}, credentials);
    return axios.post('http://localhost:3001/sessions/create', userToLogin);
  }

  static registerUser(registration) {
    let userToRegister = Object.assign({}, registration);
    return axios.post("http://localhost:3001/users", userToRegister)
  }
}

export default AuthApi;
