import axios from 'axios'

class profileApi {
  static getBudgetsForProfile(profileId) {
    return axios({
        method: 'get',
        url: `http://localhost:3001/api/profile/${profileId}/budgets`,
        headers: { Authorization: `Bearer ${localStorage.getItem('id_token')}`}
    });
  }

  static saveBudget(profileId, budget) {
    return axios({
      method: 'put',
      url: `http://localhost:3001/api/profile/${profileId}/budgets/${budget.key}`,
      data: budget,
      headers: { Authorization: `Bearer ${localStorage.getItem('id_token')}`}
    })
  }

  static createNewBudget(profileId, name, date) {
    return axios({
      method: 'post',
      url: `http://localhost:3001/api/profile/${profileId}/budgets`,
      data: { name, date },
      headers: { Authorization: `Bearer ${localStorage.getItem('id_token')}`}
    })
  }

  static deleteBudget(profileId, budgetKey) {
    return axios({
      method: 'delete',
      url: `http://localhost:3001/api/profile/${profileId}/budgets/${budgetKey}`,
      headers: { Authorization: `Bearer ${localStorage.getItem('id_token')}`}
    })
  }
}

export default profileApi;
