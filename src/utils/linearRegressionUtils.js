import { sum, times} from 'lodash';

export const calculateRegression = (data, requiredNumberOfRegressionDataPoints) => {
  const { slope, constant } = calculateSlopeAndConstant(data);
  return times(requiredNumberOfRegressionDataPoints, (index) => slope * index + constant);
};

export const calculateSlopeAndConstant = (data) => {
  let a = data.length * sum(data.map((datum, index) => (index+1) * datum));
  let b = sum(times(data.length, (index) => index+1)) * (sum(data));
  let c = data.length * sum(times(data.length, (index) => Math.pow((index+1), 2)));
  let d = Math.pow(sum(times(data.length, (index) => index+1)), 2);
  let slope = (a - b) / (c - d);

  let e = sum(data);
  let f = slope * sum(times(data.length, (index) => index+1));
  let constant = (e - f) / data.length;

  return {
    slope,
    constant
  }
};
