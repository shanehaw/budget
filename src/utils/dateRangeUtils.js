import moment from 'moment';

export const calcDateRange = (startDate, endDate) => {
  //precondition 1: both startDate and endDate must be moment objects
  if(!moment.isMoment(startDate) || !moment.isMoment(endDate)) {
    return undefined;
  }

  //precondition 2: endDate must be after or equal to startDate
  if(startDate > endDate) {
    return undefined;
  }

  //create copies of startDate and endDate and calc range
  let begin = moment(startDate);
  let end = moment(endDate);
  let range = [begin];
  while(begin < end) {
    begin = moment(begin).add(1, 'days');
    range.push(begin);
  }
  return range;
};
