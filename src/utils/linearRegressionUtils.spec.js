import assert from 'assert';
import { calculateSlopeAndConstant, calculateRegression } from './linearRegressionUtils';

describe('linearRegressionUtils', function () {
  describe('when calculating for data (1,3), (2,5), (3,6.5)', function () {
    it('should calculate slope equal to 1.75 and constant equal to 1.3', function () {
      const { slope, constant } = calculateSlopeAndConstant([3, 5, 6.5]);
      assert.deepStrictEqual(slope, 1.75);
      assert.deepStrictEqual(constant, 1.3333333333333333);
    });

    it('should use slope and constant to calculate line values for 3 data points', function () {
      const [first, second, third] = calculateRegression([3, 5, 6.5], 3);
      assert.deepStrictEqual(first, 1.3333333333333333);
      assert.deepStrictEqual(second, 3.083333333333333);
      assert.deepStrictEqual(third, 4.833333333333333);
    });

    it('should use slope and constant to calculate line values for 6 data points', function () {
      const [first, second, third, fourth, fifth, sixth] = calculateRegression([3, 5, 6.5], 6);
      assert.deepStrictEqual(first, 1.3333333333333333);
      assert.deepStrictEqual(second, 3.083333333333333);
      assert.deepStrictEqual(third, 4.833333333333333);
      assert.deepStrictEqual(fourth, 6.583333333333333);
      assert.deepStrictEqual(fifth, 8.333333333333334);
      assert.deepStrictEqual(sixth, 10.083333333333334);
    });
  });
});
