import assert from 'assert';
import { calcDateRange } from './dateRangeUtils';
import moment from 'moment';


describe('calcDateRange', function () {

  const datePattern = /[0-9]{4}\/[0-9]{2}\/[0-9]{2}/g;
  const expectedRangePattern = /\[(([0-9]{4}\/[0-9]{2}\/[0-9]{2})(,[0-9]{4}\/[0-9]{2}\/[0-9]{2})*)*]/g;

  function calculateDates(startDate, endDate) {
    let begin = startDate;
    let end = endDate;

    if(typeof startDate === 'string'
      && typeof endDate === 'string'
      && startDate.match(datePattern) !== null
      && endDate.match(datePattern) !== null
    ) {
      begin = moment(startDate, "YYYY/MM/DD");
      end = moment(endDate, "YYYY/MM/DD");
    }

    return {
      begin,
      end
    }
  }

  function calculateExpectation(expected) {
    if(typeof expected === "string" && expected.match(expectedRangePattern) !== null) {
      let splitable = expected.substr(1, expected.length - 2) //remove '[' and ']'
      let dateParts = splitable.split(",");
      return dateParts
        .map(part => {
          if(part.match(datePattern) !== null) {
            return moment(part, "YYYY/MM/DD");
          }
          return undefined;
        })
    }
    return expected;
  }

  function assertExpectation (startDate, endDate, expected) {
    const { begin, end } = calculateDates(startDate, endDate);
    const expectedRange = calculateExpectation(expected);
    const range = calcDateRange(begin, end);

    if(Array.isArray(range)) {
      assert.strictEqual(range.length, expectedRange.length);
      for(let i = 0; i < range.length; i++) {
        const actual = range[i];
        const expected = expectedRange[i];
        assert.deepStrictEqual(actual.diff(expected), 0)
      }
    } else {
      assert.deepStrictEqual(range, expected);
    }
  }

  it('should handle degenerate cases', function () {
    assertExpectation(undefined, undefined, undefined);
    assertExpectation(null, null, undefined);
    assertExpectation(1, 2, undefined);
    assertExpectation(",", undefined);
    assertExpectation("2018/12/31,2018//01//01", undefined);
  });

  it('should handle single day range', function () {
    assertExpectation('2019/01/01', '2019/01/01', "[2019/01/01]");
  });

  it('should handle multiple day range', function () {
    assertExpectation(
      '2019/01/01',
      '2019/01/05',
      '[2019/01/01,2019/01/02,2019/01/03,2019/01/04,2019/01/05]');

    assertExpectation(
      '2019/05/28',
      '2019/06/28',
      '[2019/05/28,2019/05/29,2019/05/30,2019/05/31,2019/06/01,2019/06/02,2019/06/03,2019/06/04,2019/06/05,2019/06/06,2019/06/07,2019/06/08,2019/06/09,2019/06/10,2019/06/11,2019/06/12,2019/06/13,2019/06/14,2019/06/15,2019/06/16,2019/06/17,2019/06/18,2019/06/19,2019/06/20,2019/06/21,2019/06/22,2019/06/23,2019/06/24,2019/06/25,2019/06/26,2019/06/27,2019/06/28]');
  });

  it('should calc for this month', function () {
    const startDate = moment("2019/06/28", "YYYY/MM/DD");
    const endDate = moment("2019/08/01", "YYYY/MM/DD");
    const range = calcDateRange(startDate, endDate);
    const fpd = Math.ceil(1000 / range.length);
    const totalamount = fpd * range.length;
    /*eslint-disable-next-line no-console*/
    console.log(`num of items = ${range.length} and food budget per day = ${fpd} and total amount = ${totalamount}`);
  });
});
