import React from 'react';
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom';

class Hack extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      url: ''
    };
    this.urlChange = this.urlChange.bind(this);
  }

  urlChange(event) {
    event.preventDefault();
    const newValue = event.target.value;
    this.setState({
      url: newValue
    });
  }

  render() {
    return (
      <div>
        <input value={this.state.url} onChange={this.urlChange}  type="text" />
        <Link to={this.state.url}>GO!</Link>
      </div>
    );
  }
}

Hack.propTypes = {
  history: PropTypes.object.isRequired
};

export default Hack;
