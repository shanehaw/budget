/* eslint-disable import/no-named-as-default */
import { Route, Switch } from "react-router-dom";

import HomePage from "./HomePage";
import NotFoundPage from "./NotFoundPage";
import RegistrationPage from './RegistrationPage';
import ProfilePage from './ProfilePage';
import BudgetPage from './BudgetPage';
import CreateBudgetPage from './CreateBudgetPage';
import BankReconciliationPage from './BankReconciliationPage';
import LoginNavBar from './Authorization/LoginNavBar';
//import Hack from './Hack';
import ProtectedRoute from './ProtectedRoute';
import ManageVariableExpensePage from './ManageVariableExpensePage';
import PropTypes from "prop-types";
import React from "react";
import { hot } from "react-hot-loader";

// This is a class-based component because the current
// version of hot reloading won't hot reload a stateless
// component at the top-level.

class App extends React.Component {
  render() {
    return (
      <div>
        <LoginNavBar history={this.props.history}/>
        {/*<Hack history={this.props.history} />*/}
        <div className="container">
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/register" component={RegistrationPage} />
            <ProtectedRoute path="/profile/:id/create_budget" component={CreateBudgetPage} />
            <ProtectedRoute path="/profile/:id/budget/:budgetKey/variable-expense/:variableExpenseId" component={ManageVariableExpensePage} />
            <ProtectedRoute path="/profile/:id/budget/:budgetKey/bank-reconciliation" component={BankReconciliationPage} />
            <ProtectedRoute path="/profile/:id/budget/:budgetKey" component={BudgetPage} />
            <ProtectedRoute path="/profile/:id" component={ProfilePage} />
            <Route component={NotFoundPage} />
          </Switch>
        </div>
        <footer className="footer">
          <span className="text-muted">&copy; Shane Haw 2019</span>
        </footer>
      </div>
    );
  }
}

App.propTypes = {
  history: PropTypes.object,
  children: PropTypes.element
};

export default hot(module)(App);
