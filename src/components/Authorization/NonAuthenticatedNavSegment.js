import React from 'react';
import PropTypes from 'prop-types';
import LoginForm from "./LoginForm";
import Register from './Register';

const NonAuthenticatedNavSegment = ({ updateUser, user, loginUser, registerNewUser}) => {
  return (
    <form className="form-inline ml-auto">
      <LoginForm
        updateUser={updateUser}
        user={user}
        loginUser={loginUser} />
      <Register registerNewUser={registerNewUser} />
    </form>
  );
};

NonAuthenticatedNavSegment.propTypes = {
  updateUser: PropTypes.func.isRequired,
  loginUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired,
  registerNewUser: PropTypes.func.isRequired
};

export default NonAuthenticatedNavSegment;
