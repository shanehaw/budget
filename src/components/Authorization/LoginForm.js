import React from 'react'
import PropTypes from 'prop-types';

const LoginForm = ({ updateUser, loginUser, user }) => {
  return (
      <div id="loginForm" className='form-group' >
        <label className="mr-sm-2" htmlFor="username">Username: </label>
        <div className="field mr-sm-2" >
          <input
            name="username"
            type='text'
            className="form-control"
            onChange={updateUser}
            value={user.username}
            placeholder='Username'/>
        </div>
        <label htmlFor="password" className="mr-sm-2">Password: </label>
        <div className="field mr-sm-2">
          <input
            type='password'
            name='password'
            className="form-control"
            value={user.password}
            onChange={updateUser}
            placeholder='Password'/>
        </div>
        <div className="field mr-sm-2">
          <button onClick={loginUser} className="btn btn-primary">
            Login
          </button>
        </div>
      </div>
  );
};

LoginForm.propTypes = {
  errorMessage: PropTypes.string,
  updateUser: PropTypes.func.isRequired,
  loginUser: PropTypes.func.isRequired,
  user: PropTypes.object.isRequired
};

export default LoginForm;
