import React from 'react';
import PropTypes from 'prop-types';

const Register = ({ registerNewUser }) => {
  return (
    <button onClick={registerNewUser} className="btn btn-primary mr-sm-2">Register</button>
  );
};

Register.propTypes = {
  registerNewUser: PropTypes.func.isRequired
};

export default Register;
