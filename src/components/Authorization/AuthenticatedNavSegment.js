import React from 'react';
import PropTypes from 'prop-types';
import Welcome from './Welcome';
import Logout from './Logout';

const AuthenticatedNavSegment = ({username, userid, onLogoutClick}) => {
  return (
    <div>
      <Welcome username={username} userid={userid}/>
      <Logout onLogoutClick={onLogoutClick}/>
    </div>
  );
};

AuthenticatedNavSegment.propTypes = {
  username: PropTypes.string.isRequired,
  userid: PropTypes.number.isRequired,
  onLogoutClick: PropTypes.func.isRequired
};

export default AuthenticatedNavSegment;
