import React from 'react';
import PropTypes from 'prop-types';

const Logout = ({ onLogoutClick }) => {
  return (
    <button onClick={onLogoutClick} className="btn btn-primary">
      Logout
    </button>
  );
};

Logout.propTypes = {
  onLogoutClick: PropTypes.func.isRequired
};

export default Logout;
