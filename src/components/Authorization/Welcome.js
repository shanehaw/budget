import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

const Welcome = ({username, userid}) => {
  return (
    <span style={{marginRight: 20}}>
      Welcome <Link to={`/profile/${userid}`}>{username}!</Link>
    </span>
  );
};

Welcome.propTypes = {
  username: PropTypes.string.isRequired,
  userid: PropTypes.number.isRequired
};

export default Welcome;
