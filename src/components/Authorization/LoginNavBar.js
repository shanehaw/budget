import React from 'react';
import PropTypes from 'prop-types';
import NonAuthenticatedNavSegment from './NonAuthenticatedNavSegment';
import AuthenticatedNavSegment from './AuthenticatedNavSegment';
import toastr from "toastr";
import { bindActionCreators } from 'redux';
import * as loginActions from "../../actions/authActions";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';

class LoginNavBar extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      user: Object.assign({}, this.props.user)
    };

    this.loginUser = this.loginUser.bind(this);
    this.updateUser = this.updateUser.bind(this);
    this.logoutUser = this.logoutUser.bind(this);
    this.registerNewUser = this.registerNewUser.bind(this);
  }

  static getDerivedStateFromProps(props, state) {
    if (props.user.userid !== state.user.userid) {
      return {
        user: Object.assign({}, props.user)
      };
    }
    return null;
  }

  loginUser(event) {
    event.preventDefault();
    const user = {
      username: this.state.user.username,
      password: this.state.user.password
    };
    this.props.actions.loginUser(user)
      .then((userid) => {
        this.props.history.push(`/profile/${userid}`);
      }).catch(err => {
        toastr.error(err);
      })
  }

  updateUser(event) {
    const field = event.target.name;
    let user = Object.assign({}, this.state.user);
    user[field] = event.target.value;
    this.setState({ user });
  }

  logoutUser() {
    this.props.actions.logoutUser();
    this.props.history.push('/');
  }

  registerNewUser(event) {
    event.preventDefault();
    this.props.history.push('/register');
  }

  render() {
    const { isAuthenticated } = this.props;
    const { user } = this.state;

    return (
      <nav className='navbar navbar-default'>
        <div className='container-fluid'>
          <Link className="navbar-brand" to="/">Budget</Link>
          <div className='navbar-form'>

            {!isAuthenticated &&
              <NonAuthenticatedNavSegment
                user={user}
                updateUser={this.updateUser}
                loginUser={this.loginUser}
                registerNewUser={this.registerNewUser} />
            }

            {isAuthenticated &&
              <AuthenticatedNavSegment
                username={user.username}
                userid={user.userid}
                onLogoutClick={this.logoutUser} />
            }

          </div>
        </div>
      </nav>
    );
  }
}

LoginNavBar.propTypes = {
  history: PropTypes.object,
  user: PropTypes.object.isRequired,
  isAuthenticated: PropTypes.bool.isRequired,
  actions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const { user, isAuthenticated } = state.auth;
  if(isAuthenticated && (!user.username || user.username.length === 0)) {
    console.log(state); /* eslint-disable-line no-console */
  }
  return {
    ...ownProps,
    user: state.auth.user,
    isAuthenticated: state.auth.isAuthenticated
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(loginActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginNavBar);
