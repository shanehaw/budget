import React from 'react';
import PropTypes from 'prop-types';

const ServersideFormError = ({ error }) => {
  return (
    <div className="form-group form-group-sm col-sm-12">
      <div className="row">
        <div className="offset-sm-3 col-sm-6">
          <span style={{color:"red", fontWeight:"bold"}}>{error}</span>
        </div>
      </div>
    </div>
  );
};

ServersideFormError.propTypes = {
  error: PropTypes.string.isRequired
};

export default ServersideFormError;
