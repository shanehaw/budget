import React from 'react';
import PropTypes from 'prop-types';

const TextInput = ({ id, name, label, type, value, error, onChange }) => {
  const hasError = error && error.length > 0;
  return (
    <div className="form-group form-group-sm col-sm-12">
      <div className="row">
        <label htmlFor={name} className="col-sm-3 col-form-label">{label}</label>
        <div className="col-sm-5">
          <input onChange={onChange} type={type} className="form-control" id={id} name={name} value={value} />
        </div>
        {hasError && <span className="col-sm-4" style={{color:"red", fontWeight:"bold"}}>{error}</span>}
      </div>
    </div>
  );
};

TextInput.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  error: PropTypes.string,
  onChange: PropTypes.func.isRequired
};

export default TextInput;
