import React from 'react';
import PropTypes from 'prop-types';

const SubmitButton = ({ text, onClick, disabled, bootstrapClassName }) => {
  return (
    <div className="form-group form-group-sm col-sm-12">
      <div className="row">
        <div className={bootstrapClassName}>
          <button disabled={disabled} onClick={onClick} type="submit" className="btn btn-primary">{text}</button>
        </div>
      </div>
    </div>
  )
};

SubmitButton.propTypes = {
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  bootstrapClassName: PropTypes.string
};

export default SubmitButton;
