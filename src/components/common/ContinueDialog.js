import React from 'react';
import PropTypes from 'prop-types';
import {Modal, Button} from 'react-bootstrap';

export const ContinueDialog = ({question, show, onCancel, onDiscardAndContinue, onSaveAndContinue}) => {
  return (
    <Modal show={show} onHide={onCancel}>
      <Modal.Header closeButton>
        <Modal.Title>Continue?</Modal.Title>
      </Modal.Header>
      <Modal.Body>{question}</Modal.Body>
      <Modal.Footer>
        <Button variant="danger" onClick={onCancel}>
          Cancel
        </Button>
        <Button variant="secondary" onClick={onDiscardAndContinue}>
          Discard and Continue
        </Button>
        <Button variant="primary" onClick={onSaveAndContinue}>
          Save and Continue
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

ContinueDialog.propTypes = {
  question: PropTypes.string.isRequired,
  show: PropTypes.bool.isRequired,
  onCancel: PropTypes.func.isRequired,
  onDiscardAndContinue: PropTypes.func.isRequired,
  onSaveAndContinue: PropTypes.func.isRequired
};
