import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment'
import './BudgetList.css';

const BudgetList = ({budgets, loadBudget, deleteBudget, duplicateBudgetToNextMonth}) => {
  function buildBudgetCard(b) {
    return <div key={b.key} className="col-sm-12 col-md-6 col-lg-4 budget-card">
      <form>
        <div className="card">
          <div className="card-body">
            <span style={{fontWeight: 'bold'}}>{b.name}</span><br/>
            <br/>
            <span>{moment(b.date, 'YYYY/MM/DD').format('YYYY/MM/DD')}</span>
          </div>
          <div className="card-footer">
              <div className="row">
                <button type="button" className="btn btn-primary col-12 mb-1" id={b.name} onClick={(e) => loadBudget(e, b.key)}>Load</button>
                <button type="button" className="btn btn-success col-12 mb-1" id={b.name} onClick={(e) => duplicateBudgetToNextMonth(e, b.key)}>Duplicate To Next Month</button>
                <button type="button" className="btn btn-danger col-12 mb-1" onClick={(e) => deleteBudget(e, b.key)}>Delete</button>
              </div>
          </div>
        </div>
      </form>
    </div>;
  }

  return (
    <div className="row mt-3">
      { budgets.map(b => buildBudgetCard(b)) }
    </div>
  );
};

BudgetList.propTypes = {
  budgets: PropTypes.array.isRequired,
  loadBudget: PropTypes.func,
  deleteBudget: PropTypes.func,
  duplicateBudgetToNextMonth: PropTypes.func
};

export default BudgetList;
