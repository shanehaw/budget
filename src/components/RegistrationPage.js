import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as registrationActions from '../actions/registrationActions';
import RegistrationForm from './Registration/RegistrationForm';
import toastr from "toastr";

class RegistrationPage extends React.Component {
  constructor(props, context) {
    super(props, context);

    this.state = {
      isAttemptingRegistration: false,
      registeringUser: Object.assign({}, this.props.registeringUser),
      errors: {
        usernameError: '',
        password1Error: '',
        password2Error: '',
        serversideError: ''
      }
    };

    this.onRegistrationValueChange = this.onRegistrationValueChange.bind(this);
    this.onRegistration = this.onRegistration.bind(this);
  }

  onRegistrationValueChange(event) {
    const field = event.target.name;
    let registeringUser = Object.assign({}, this.state.registeringUser);
    registeringUser[field] = event.target.value;
    this.setState({ registeringUser });
  }

  isRegistrationFormValid() {
    const { registeringUser } = this.state;
    let errors = {
      usernameError: '',
      password1Error: '',
      password2Error: '',
      serversideError: ''
    };
    let isValid = true;

    //Rule 1: username must be at least 3 characters
    if(registeringUser.username.length < 3) {
      errors.usernameError = 'username must be at least 3 characters.';
      isValid = false;
    }

    //Rule 2: password must be at least characters
    if(registeringUser.password.length < 3) {
      errors.password1Error = 'password must be at least 3 characters.';
      isValid = false;
    }

    //Rule 3: password1 and password2 must be the same
    if(registeringUser.password !== registeringUser.password2) {
      errors.password2Error = 'passwords do not match';
      isValid = false;
    }

    this.setState({
      errors
    });
    return isValid;
  }

  onRegistration(event) {
    event.preventDefault();
    this.setState({isAttemptingRegistration: true});
    if(!this.isRegistrationFormValid()) {
      this.setState({isAttemptingRegistration: false});
      return;
    }
    this.attemptRegistration();
  }

  handleRegistrationSuccess(userid) {
    this.setState({isAttemptingRegistration: false});
    this.props.history.push(`/profile/${userid}`);
    toastr.success('Successfully registered');
  }

  handleRegistrationServerError(err) {
    this.setState({
      isAttemptingRegistration: false,
      errors: {
        usernameError: '',
        password1Error: '',
        password2Error: '',
        serversideError: err
      }
    });
  }

  attemptRegistration() {
    const { registeringUser } = this.state;
    this.props.actions.register(registeringUser)
      .then(userid => this.handleRegistrationSuccess(userid))
      .catch(err => this.handleRegistrationServerError(err));
  }

  render() {
    const { errors, registeringUser, isAttemptingRegistration } = this.state;
    return (
      <div>
        <div className="jumbotron text-center">
          <h1>Registering as a new user</h1>
        </div>
        <div className="container">
          <RegistrationForm
            errors={errors}
            registeringUser={registeringUser}
            registerUser={this.onRegistration}
            onRegistrationValueChange={this.onRegistrationValueChange}
            isAttemptingRegistration={isAttemptingRegistration} />
        </div>
      </div>
    );
  }
}

RegistrationPage.propTypes = {
  isRegistering: PropTypes.bool.isRequired,
  registeringUser: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  return {
    ...ownProps,
    isRegistering: state.registration.isRegistering,
    registeringUser: state.registration.registeringUser
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(registrationActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationPage);
