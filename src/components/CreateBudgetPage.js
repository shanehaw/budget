import React from 'react'
import { CreateBudgetForm } from './Budget/CreateBudgetForm';
import * as profileActions from "../actions/profileActions";
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import PropTypes from "prop-types";
import { Link } from 'react-router-dom';

class CreateBudgetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isAttemptingBudgetCreation: false,
      budgetname: '',
      budgetDate: '',
      errors: {
        budgetnameError: '',
        budgetDateError: '',
        serversideError: ''
      }
    };
    this.createBudget = this.createBudget.bind(this);
    this.onBudgetCreationValueChange = this.onBudgetCreationValueChange.bind(this);
  }

  createBudget(e) {
    e.preventDefault();
    const { profileId } = this.props;
    const { budgetname, budgetDate } = this.state;
    this.props.actions.createNewBudget(profileId, budgetname, budgetDate)
      .then(() => {
          this.props.actions.getProfileBudgets(profileId)
            .then(() => this.props.history.push(`/profile/${profileId}`))
      });
  }

  onBudgetCreationValueChange(e) {
    const field = e.target.name;
    const newState = {...this.state};
    newState[field] = e.target.value;
    this.setState(newState);
  }

  render() {
    const { isAttemptingBudgetCreation, budgetname, budgetDate, errors } = this.state;
    const { accessAllowed, isProfileLoading, profileId } = this.props;

    let content = undefined;

    if(isProfileLoading) {
      content = <span>Loading...</span>
    } else if(!accessAllowed) {
      const link = profileId === 0 ? `/` : `/profile/${profileId}`;
      const text = profileId === 0 ? 'Home' : 'your profile';
      content = <p>Either this profile does not exist, or you do not have access to it. Please <Link to={link}>click here</Link> to return to {text}.</p>;
    } else {
      content = <CreateBudgetForm
                    isAttemptingBudgetCreation={isAttemptingBudgetCreation}
                    budgetname={budgetname}
                    createBudget={this.createBudget}
                    onBudgetCreationValueChange={this.onBudgetCreationValueChange}
                    errors={errors}
                    budgetDate={budgetDate} />
    }

    return (
      <div>
        <div className="jumbotron text-center">
          <h1>Create Budget</h1>
        </div>
        <div className="container">
          { content }
        </div>
      </div>
    );
  }
}

CreateBudgetPage.propTypes = {
  actions: PropTypes.object.isRequired,
  profileId: PropTypes.number.isRequired,
  history: PropTypes.object.isRequired,
  accessAllowed: PropTypes.bool.isRequired,
  isProfileLoading: PropTypes.bool.isRequired,
};

function mapStateToProps(state, ownProps) {
  const { accessAllowed, isProfileLoading } = ownProps.protectedRouteProps;
  return {
    ...ownProps,
    accessAllowed,
    isProfileLoading,
    profileId: state.auth.user.userid
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(profileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateBudgetPage);
