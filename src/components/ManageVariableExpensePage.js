import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as profileActions from '../actions/profileActions';
import {cloneDeep, find, findIndex, maxBy, remove} from 'lodash';
import {VariableExpenseList} from './ManageVariableExpense/VariableExpenseList';
import {VariableExpenseControlPanel} from './ManageVariableExpense/VariableExpenseControlPanel';
import {VariableExpenseProjection} from './ManageVariableExpense/VariableExpenseProjection';
import moment from 'moment';
import {Link} from "react-router-dom";
import toastr from "toastr";

class ManageVariableExpensePage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      budget: cloneDeep(props.budget),
      variableExpense: cloneDeep(props.variableExpense),
      variableExpenseIsFound: typeof props.variableExpense !== 'undefined',
      saveBudgetDisabled: false
    };
    this.expenseItemUpdated = this.expenseItemUpdated.bind(this);
    this.deleteExpenseItem = this.deleteExpenseItem.bind(this);
    this.addExpenseItem = this.addExpenseItem.bind(this);
    this.saveBudget = this.saveBudget.bind(this);
    this.handleBudgetSaveSuccess = this.handleBudgetSaveSuccess.bind(this);
    this.getNotFoundContent = this.getNotFoundContent.bind(this);
    this.getFoundContent = this.getFoundContent.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(typeof nextProps.variableExpense !== typeof prevState.variableExpense) {
      return {
        budget: cloneDeep(nextProps.budget),
        variableExpense: cloneDeep(nextProps.variableExpense),
        variableExpenseIsFound: typeof nextProps.variableExpense !== 'undefined'
      }
    }
    return null
  }

  expenseItemUpdated(e, id, prop) {
    const { variableExpense } = this.state;
    const item = find(variableExpense.currentItems, { id });
    if(item) {
      item[prop] = prop !== 'amount' || e.target.value.length === 0
        ? e.target.value
        : parseFloat(e.target.value);
      this.setState({ variableExpense });
    }
  }

  deleteExpenseItem(e, id) {
    e.preventDefault();
    const { variableExpense } = this.state;
    remove(variableExpense.currentItems, (e) => e.id === id);
    this.setState({ variableExpense });
  }

  addExpenseItem(e) {
    e.preventDefault();
    const { variableExpense, budget } = this.state;
    const date = moment(budget.date, "YYYY/MM/DD").startOf('month')
    const maxCurrentItem = maxBy(variableExpense.currentItems, 'id') || {id: 0};
    const newExpenseItem = {
      id: maxCurrentItem.id  + 1,
      name: 'New Item',
      date: date.format("YYYY/MM/DD"),
      amount: 0
    };
    variableExpense.currentItems.push(newExpenseItem);
    this.setState({ variableExpense });
  }

  saveBudget(e) {
    e.preventDefault();
    this.setState({ saveBudgetDisabled: true}, function() {
      const { budget, variableExpense } = this.state;
      const { profileId } = this.props;
      const index = findIndex(budget.expenses.variable, {id: variableExpense.id});
      budget.expenses.variable[index] = variableExpense;
      this.props.profileActions.saveBudget(profileId, budget)
        .then(() => this.handleBudgetSaveSuccess())
        .catch((err) => toastr.error(`An error occurred saving your budget: ${err}`))
        .finally(() => this.setState({ saveBudgetDisabled: false}));
    });

  }

  handleBudgetSaveSuccess() {
    const { profileId, budgetKey } = this.props;
    this.props.profileActions.getProfileBudgets(profileId)
      .then(() => this.props.history.push(`/profile/${profileId}/budget/${budgetKey}`));
  }

  handleBudgetSaveFailure(err) {
    alert(`Error: ${err}`)
  }

  getFoundContent() {
    const { variableExpense, saveBudgetDisabled, budget } = this.state;
    return (
      <div>
        <VariableExpenseControlPanel
          addExpenseItem={this.addExpenseItem}
          saveBudget={this.saveBudget}
          saveBudgetDisabled={saveBudgetDisabled} />
        <VariableExpenseList
          items={variableExpense.currentItems}
          expenseItemUpdated={this.expenseItemUpdated}
          deleteExpenseItem={this.deleteExpenseItem} />
        <VariableExpenseProjection expense={variableExpense} budgetDate={budget.date} />
      </div>
    );
  }

  getNotFoundContent() {
    const { profileId } = this.props;
    const link = profileId === 0 ? `/` : `/profile/${profileId}`;
    const text = profileId === 0 ? 'Home' : 'your profile';
    return <p>Either this expense does not exist, or you do not have access to it. Please <Link to={link}>click here</Link> to return to {text}.</p>;
  }

  render() {
    const { variableExpense, variableExpenseIsFound } = this.state;
    const { accessAllowed, isProfileLoading } = this.props;

    let content = undefined;
    let heading = undefined;

    if(isProfileLoading) {
      heading = <h1>Manage a Variable Expense Page</h1>;
      content = <span>Loading...</span>;
    } else {
      heading = accessAllowed && variableExpenseIsFound
        ? <h1>Managing {variableExpense.name}</h1>
        : <h1>Unknown Expense</h1>;

      content = accessAllowed && variableExpenseIsFound
        ? this.getFoundContent()
        : this.getNotFoundContent()
    }

    return (
      <div>
        <div className="jumbotron text-center">
          { heading }
        </div>
        { content }
      </div>);
  }
}

ManageVariableExpensePage.propTypes = {
  profileId: PropTypes.number,
  budgetKey: PropTypes.string,
  variableExpense: PropTypes.object,
  accessAllowed: PropTypes.bool.isRequired,
  isProfileLoading: PropTypes.bool.isRequired,
  budget: PropTypes.object,
  profileActions: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const { selectedBudget, accessAllowed, isProfileLoading } = ownProps.protectedRouteProps;
  const { budgetKey, variableExpenseId } = ownProps.match.params;
  const budget = cloneDeep(selectedBudget);
  let variableExpense = undefined;
  if(selectedBudget) {
    variableExpense = find(budget.expenses.variable || [], {'id': Number.parseInt(variableExpenseId) });
  }
  return {
    profileId: state.auth.user.userid,
    budgetKey,
    variableExpense,
    budget,
    accessAllowed,
    isProfileLoading,
    history: ownProps.history
  }
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch)
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(ManageVariableExpensePage);
