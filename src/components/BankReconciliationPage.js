import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileActions from '../actions/profileActions';
import { cloneDeep, round, filter, findIndex } from 'lodash'
import {Link} from "react-router-dom";
import AutosizeInput from 'react-input-autosize';
import toastr from "toastr";

const budgetActualName = "Budget Expected Actual Amount";

class BankReconciliationPage extends React.Component {

  constructor(props) {
    super(props);
    this.state = BankReconciliationPage.createState(props.selectedBudget);
    this.getNotFoundContent = this.getNotFoundContent.bind(this);
    this.addDebit = this.addDebit.bind(this);
    this.addCredit = this.addCredit.bind(this);
    this.createBudgetRow = this.createBudgetRow.bind(this);
    this.goBackToBudget = this.goBackToBudget.bind(this);
    this.saveBudget = this.saveBudget.bind(this);
    this.removeDebit = this.removeDebit.bind(this);
    this.removeCredit = this.removeCredit.bind(this);
  }

  static createState(selectedBudget) {
    let budget = cloneDeep(selectedBudget);
    let debits = ((budget || {}).reconciliation || {}).debits || []
    let credits = ((budget || {}).reconciliation || {}).credits || []
    return {
      budgetIsFound: typeof selectedBudget !== 'undefined',
      budget,
      debits,
      credits
    };
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(typeof nextProps.selectedBudget !== typeof prevState.budget) {
      return BankReconciliationPage.createState(nextProps.selectedBudget);
    }
    return null
  }

  getNotFoundContent() {
    const {userId} = this.props;
    const link = userId === 0 ? `/` : `/profile/${userId}`;
    const text = userId === 0 ? 'Home' : 'your profile';
    return <p>Either this budget does not exist, or you do not have access to it. Please <Link to={link}>click here</Link> to return to {text}.</p>;
  }

  calculateBudgetActualTotal(budget) {
    let actualTally = 0;
    for (let i = 0; i < budget.income.length; i++) {
      const incomeItem = budget.income[i];
      actualTally += incomeItem.actual;
    }

    for (let i = 0; i < budget.expenses.fixed.length; i++) {
      const fixedExpense = budget.expenses.fixed[i];
      actualTally -= fixedExpense.actual;
    }

    for (let i = 0; i < budget.expenses.variable.length; i++) {
      const variableExpense = budget.expenses.variable[i];
      for (let j = 0; j < variableExpense.currentItems.length; j++) {
        const currentItem = variableExpense.currentItems[j];
        actualTally -= currentItem.amount;
      }
    }

    return actualTally;
  }

  calculateRows(debits, credits) {
    let result = [];
    let debitTotal = 0.0;
    let creditTotal = 0.0;
    const maxItemCount = Math.max(debits.length, credits.length);
    for(let i = 0; i < maxItemCount; i++) {
      let curDebit, curCredit;
      if(i < debits.length) {
        curDebit = debits[i];
        debitTotal += round(curDebit.amount, 2);
      }
      if(i < credits.length) {
        curCredit = credits[i];
        creditTotal += round(curCredit.amount, 2);
      }
      result.push(this.createBudgetRow(curDebit, curCredit, i))
    }

    if(debitTotal > creditTotal) {
      const diff = round((debitTotal - creditTotal), 2);
      creditTotal = debitTotal;
      result.push(this.createBudgetRow(undefined, {name:"Balance", amount:diff}, maxItemCount+1, false));
    } else if (creditTotal > debitTotal) {
      const diff = round((creditTotal - debitTotal), 2);
      debitTotal = creditTotal;
      result.push(this.createBudgetRow({name:"Balance", amount:diff}, undefined, maxItemCount+1, false));
    }
    result.push(this.createBudgetTotalRow({amount:debitTotal},{amount:creditTotal}, maxItemCount+3));
    return result;
  }

  createBudgetRow(debit, credit, index, deleteAble = true) {
    return (
      <tr key={index}>
        <td className="w-25">
          { typeof debit !== "undefined"
            ? typeof debit.name !== "undefined"
              ? debit.editable
                ? <AutosizeInput inputStyle={{border: 'none'}} type="text" value={debit.name} onChange={(e) => this.updateDebit(e.target.value, debit.amount, debit.id)} />
                : <span>{debit.name}</span>
              : <span>&nbsp;</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{textAlign: "right"}}>
          { typeof debit !== "undefined"
            ? typeof debit.amount !== "undefined"
              ? debit.editable
                ? <>
                    <span className="euros">
                        <AutosizeInput inputStyle={{border: 'none'}} type="text" value={debit.amount} onChange={(e) => this.updateDebit(debit.name, e.target.value, debit.id)} />
                    </span>
                    { deleteAble ? <button className="btn btn-outline-danger ml-2" onClick={(e) => this.removeDebit(e, debit.id)}>X</button> : <span>&nbsp;</span> }
                  </>
                : <span className="euros">{round(debit.amount, 2).toFixed(2)}</span>
              : <span className="euros">0</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{borderLeft: "1px solid"}}>
          { typeof credit !== "undefined"
            ? typeof credit.name !== "undefined"
              ? credit.editable
                ? <AutosizeInput inputStyle={{border: 'none'}} type="text" value={credit.name} onChange={(e) => this.updateCredit(e.target.value, credit.amount, credit.id)} />
                : <span>{credit.name}</span>
              : <span>&nbsp;</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{textAlign: "right"}}>
          { typeof credit !== "undefined"
            ? typeof credit.amount !== "undefined"
              ? credit.editable
                ? <>
                    <span className="euros">
                        <AutosizeInput inputStyle={{border: 'none'}} type="text" value={credit.amount} onChange={(e) => this.updateCredit(credit.name, e.target.value, credit.id)} />
                    </span>
                    { deleteAble ? <button className="btn btn-outline-danger ml-2" onClick={(e) => this.removeCredit(e, credit.id)}>X</button> : <span>&nbsp;</span> }
                  </>
                : <span className="euros">{round(credit.amount, 2).toFixed(2)}</span>
              : <span className="euros">0</span>
            : <span>&nbsp;</span>
          }
        </td>
      </tr>
    )
  }

  createBudgetTotalRow(debit, credit, index) {
    return (
      <tr key={index}>
        <td className="w-25">
          { typeof debit !== "undefined"
            ? typeof debit.name !== "undefined"
              ? <span>{debit.name}</span>
              : <span>&nbsp;</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{textAlign: "right", borderTop: "solid 1px", borderBottom: "double 3px"}}>
          { typeof debit !== "undefined"
            ? typeof debit.amount !== "undefined"
              ? <span className="euros">{round(debit.amount, 2).toFixed(2)}</span>
              : <span className="euros">0</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{borderLeft: "1px solid"}}>
          { typeof credit !== "undefined"
            ? typeof credit.name !== "undefined"
              ? <span>{credit.name}</span>
              : <span>&nbsp;</span>
            : <span>&nbsp;</span>
          }
        </td>
        <td className="w-25" style={{textAlign: "right", borderTop: "solid 1px", borderBottom: "double 3px"}}>
          { typeof credit !== "undefined"
            ? typeof credit.amount !== "undefined"
              ? <span className="euros">{round(credit.amount, 2).toFixed(2)}</span>
              : <span className="euros">0</span>
            : <span>&nbsp;</span>
          }
        </td>
      </tr>
    )
  }

  removeDebit(e, id) {
    e.preventDefault();
    const { debits } = this.state;
    const index = findIndex(debits, { id });
    debits.splice(index, 1);
    /* eslint-disable-line no-console */ console.log(debits);
    this.setState({
      debits: debits.splice(index, 1)
    });
  }

  removeCredit(e, id) {
    e.preventDefault();
    const { credits } = this.state;
    const index = findIndex(credits, { id });
    credits.splice(index, 1);
    /* eslint-disable-line no-console */ console.log(credits);
    this.setState({
      credits: credits.splice(index, 1)
    });
  }

  addDebit(event) {
    event.preventDefault();
    const { debits } = this.state;
    let maxId = 0;
    for(let i = 0; i < debits.length; i++) {
      if(debits[i].id > maxId) {
        maxId = debits[i].id;
      }
    }
    debits.push({
      name: "New Debit",
      amount: 0,
      editable: true,
      id: maxId + 1
    });
    this.setState({
      debits
    });
  }

  addCredit(event) {
    event.preventDefault();
    const { credits } = this.state;
    let maxId = 0;
    for(let i = 0; i < credits.length; i++) {
      if(credits[i].id > maxId) {
        maxId = credits[i].id;
      }
    }
    credits.push({
      name: "New Credit",
      amount: 0,
      editable: true,
      id: maxId + 1
    });
    this.setState({
      credits
    });
  }

  goBackToBudget(event) {
    event.preventDefault();
    const { userId, selectedBudget } = this.props;
    this.props.history.push(`/profile/${userId}/budget/${selectedBudget.key}`);
  }

  saveBudget(event) {
    event.preventDefault();
    const { budget, debits, credits } = this.state;
    const { userId } = this.props;

    const debitsToSave = filter(debits, d => d.id !== 0);
    const creditsToSave = filter(credits, d => d.id !== 0);

    budget.reconciliation =  {
      debits: debitsToSave,
      credits: creditsToSave
    };

    this.props.profileActions.saveBudget(userId, budget)
      .then(() => toastr.success("Successfully saved budget!"))
      .catch((err) => toastr.error(`An error occurred saving your budget: ${err}`))
      .then(() => this.props.profileActions.getProfileBudgets(userId));
  }

  getBudgetFoundContent() {
    let { debits, credits, budget } = this.state;

    const currentBalance = this.calculateBudgetActualTotal(budget);
    if(currentBalance > 0) {
      debits = [{
        name: budgetActualName,
        amount: currentBalance,
        editable: false,
        id: 0
      }, ...debits];
    } else {
      credits = [{
        name: budgetActualName,
        amount: Math.abs(currentBalance),
        editable: false,
        id: 0
      }, ...credits];
    }

    return (
      <>
        <div className="row">
          <div className="col-12 mb-2">
            <button className="btn btn-outline-info" onClick={this.goBackToBudget}>Back To Budget</button>
            <button className="btn btn-success float-right mr-2" onClick={this.saveBudget}>Save Changes</button>
            <button className="btn btn-info float-right mr-2" onClick={this.addCredit} >Add Credit</button>
            <button className="btn btn-info float-right mr-2" onClick={this.addDebit}>Add Debit</button>
          </div>
        </div>
        <table className="table">
          <tbody>
          {this.calculateRows(debits, credits)}
          </tbody>
        </table>
      </>
    )
  }

  updateDebit(name, amount, id) {
    const { debits } = this.state;
    this.setItemInArray(debits, name, amount, id);
    this.setState({ debits });
  }

  updateCredit(name, amount, id) {
    const { credits  } = this.state;
    this.setItemInArray(credits, name, amount, id);
    this.setState({ credits });
  }

  setItemInArray(items, name, amount, id) {
    let wasSet = false;
    let maxId = 0;

    for(let i = 0; i < items.length; i++) {
      if(items[i].id > maxId) maxId = items[i].id;
      if(items[i].name === name) {
        items[i].amount = amount;
        wasSet = true;
        break;
      }
      if(items[i].id === id) {
        items[i].amount = amount;
        items[i].name = name;
        wasSet = true;
        break;
      }
    }

    if(!wasSet) {
      const editable = name !== budgetActualName;
      const id = maxId + 1;
      items.push({
        name,
        amount,
        editable,
        id
      });
    }
  }

  render() {
    const { accessAllowed, isProfileLoading } = this.props;
    const { budgetIsFound } = this.state;

    let content;
    if (isProfileLoading) {
      content = <span>Loading...</span>;
    } else {
      content = accessAllowed && budgetIsFound
        ? this.getBudgetFoundContent()
        : this.getNotFoundContent();
    }

    return (
      <div>
        <div className="jumbotron text-center">
          <h1>Bank Reconciliation Page</h1>
        </div>
        {content}
      </div>
    )
  }
}

BankReconciliationPage.propTypes = {
  isProfileLoading: PropTypes.bool.isRequired,
  accessAllowed: PropTypes.bool,
  selectedBudget: PropTypes.object,
  userId: PropTypes.number,
  history: PropTypes.object.isRequired,
  profileActions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const { selectedBudget, accessAllowed, isProfileLoading } = ownProps.protectedRouteProps;
  return {
    isProfileLoading,
    accessAllowed,
    selectedBudget,
    userId: state.auth.user.userid
  };
}

function mapDispatchToProps(dispatch) {
  return  {
    profileActions: bindActionCreators(profileActions, dispatch)
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(BankReconciliationPage);

