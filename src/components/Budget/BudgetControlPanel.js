import React from 'react';
import PropTypes from 'prop-types';

const BudgetControlPanel = ({ addIncomeItem, addFixedExpense, addVariableExpenses: addVariableExpenses, saveBudget, returnToBudgets, doBankReconciliation }) => {
  return (
    <div className="row">
      <div className="col-12 mb-2">
        <button className="btn btn-outline-primary" onClick={returnToBudgets}>
          <i className="fas fa-camera">Back to Budgets</i>
        </button>
        <button className="btn btn-info float-right" onClick={doBankReconciliation} >Bank Reconciliation</button>
      </div>
      <div className="col-12 mb-2">
        <button className="btn btn-primary" onClick={addIncomeItem}>Add Income Item</button>
        <button className="btn btn-primary ml-2" onClick={addFixedExpense}>Add Fixed Expense</button>
        <button className="btn btn-primary ml-2" onClick={addVariableExpenses}>Add Variable Expenses</button>
        <button className="btn btn-success float-right" onClick={saveBudget}>Save Budget</button>
      </div>
    </div>
  );
};

BudgetControlPanel.propTypes = {
  addIncomeItem: PropTypes.func.isRequired,
  addFixedExpense: PropTypes.func.isRequired,
  addVariableExpenses: PropTypes.func.isRequired,
  saveBudget: PropTypes.func.isRequired,
  returnToBudgets: PropTypes.func.isRequired,
  doBankReconciliation: PropTypes.func.isRequired
};

export default BudgetControlPanel;
