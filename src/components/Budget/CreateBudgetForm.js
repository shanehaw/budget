import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../common/TextInput';
import SubmitButton from '../common/SubmitButton';
import ServersideFormError from '../common/ServersideFormError';

export const CreateBudgetForm = ({ budgetname, budgetDate, errors, onBudgetCreationValueChange, isAttemptingBudgetCreation, createBudget }) => {
  const { budgetnameError, budgetDateError, serversideError } = errors;
  let hasError = serversideError && serversideError.length > 0;

  return (
    <form className="form-horizontal">
      <div className="container">
        <div className="row">
          <TextInput
            label="Budget Name:"
            type="text"
            id="createBudgetFormBudgetName"
            name="budgetname"
            value={budgetname}
            onChange={onBudgetCreationValueChange}
            error={budgetnameError} />

          <TextInput
            label="Budget Date:"
            type="text"
            id="createBudgetFormBudgetDate"
            name="budgetDate"
            value={budgetDate}
            onChange={onBudgetCreationValueChange}
            error={budgetDateError} />

          {hasError && <ServersideFormError error={serversideError} />}

          <SubmitButton
            disabled={isAttemptingBudgetCreation}
            onClick={createBudget}
            text="Register"
            bootstrapClassName="offset-sm-3 col-sm-6" />
        </div>
      </div>
    </form>
  )
};

CreateBudgetForm.propTypes = {
  budgetname : PropTypes.string.isRequired,
  budgetDate : PropTypes.string.isRequired,
  errors : PropTypes.object,
  onBudgetCreationValueChange : PropTypes.func.isRequired,
  isAttemptingBudgetCreation : PropTypes.bool.isRequired,
  createBudget : PropTypes.func.isRequired
};
