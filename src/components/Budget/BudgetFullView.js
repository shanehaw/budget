import React from 'react';
import PropTypes from 'prop-types';
import './BudgetFullView.css';
import AutosizeInput from 'react-input-autosize';

export const BudgetFullView = ({budget, incomeItemUpdated, fixedExpenseItemUpdated, variableExpenseItemUpdated, deleteIncomeItem, deleteFixedExpense, deleteVariableExpense, manageVariableExpense}) => {

  function createIncomeRows(income) {
    const inputStyle = {border: 'none'};
    return income.map(i =>
      <tr key={i.id}>
          <td>
          <span className="ml-5">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='text'
              onChange={(e) => incomeItemUpdated(e, i.name, "name")}
              value={i.name} />
          </span>
        </td>
        <td>
          <span className="euros">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='number'
              extraWidth={0}
              onChange={(e) => incomeItemUpdated(e, i.name, "expected")}
              value={i.expected} />
          </span>
        </td>
        <td>
          <span className="euros">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='number'
              extraWidth={0}
              onChange={(e) => incomeItemUpdated(e, i.name, "actual")}
              value={i.actual} />
          </span>
        </td>
        <td><button type="button" onClick={() => deleteIncomeItem(i.id)} className="btn btn-danger">Delete</button></td>
      </tr>
    );
  }

  function createFixedExpenses(fixedExpenses) {
    const inputStyle = {border: 'none'};
    return fixedExpenses.map(fe =>
      <tr key={fe.id}>
        <td>
          <span className="ml-5">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='text'
              onChange={(e) => fixedExpenseItemUpdated(e, fe.name, "name")}
              value={fe.name} />
          </span>
        </td>
        <td>
          <span className="euros">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='number'
              extraWidth={0}
              onChange={(e) => fixedExpenseItemUpdated(e, fe.name, "expected")}
              value={fe.expected} />
          </span>
        </td>
        <td>
          <span className="euros">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='number'
              extraWidth={0}
              onChange={(e) => fixedExpenseItemUpdated(e, fe.name, "actual")}
              value={fe.actual} />
          </span>
        </td>
        <td><button type="button" onClick={() => deleteFixedExpense(fe.id)} className="btn btn-danger">Delete</button></td>
      </tr>
    );
  }

  function createVariableExpenses(variableExpenses) {
    const inputStyle = {border: 'none'};
    return variableExpenses.map(ve => {
      let actual = 0.0;
      for(let i = 0; i < ve.currentItems.length; i++) {
        actual += ve.currentItems[i].amount;
      }

      return (
        <tr key={ve.id}>
          <td>
            <span className="ml-5">
              <AutosizeInput
                inputStyle={inputStyle}
                minWidth={5}
                type='text'
                onChange={(e) => variableExpenseItemUpdated(e, ve.name, "name")}
                value={ve.name} />
            </span>
          </td>
          <td>
            <span className="euros">
              <AutosizeInput
                inputStyle={inputStyle}
                minWidth={5}
                type='number'
                extraWidth={0}
                onChange={(e) => variableExpenseItemUpdated(e, ve.name, "expected")}
                value={ve.expected} />
            </span>
          </td>
          <td><span className="euros">{actual.toFixed(2)}</span></td>
          <td>
            <button type="button" onClick={() => manageVariableExpense(ve.id)} className="btn btn-info mr-2">Manage</button>
            <button type="button" onClick={() => deleteVariableExpense(ve.id)} className="btn btn-danger">Delete</button>
          </td>
        </tr>
      );
    });
  }

  function createTotals(budget) {
    let expectedTally = 0;
    let actualTally = 0;
    for(let i = 0; i < budget.income.length; i++) {
      const incomeItem = budget.income[i];
      expectedTally += incomeItem.expected;
      actualTally += incomeItem.actual;
    }

    for(let i = 0; i < budget.expenses.fixed.length; i++){
      const fixedExpense = budget.expenses.fixed[i];
      expectedTally -= fixedExpense.expected;
      actualTally -= fixedExpense.actual;
    }

    for(let i = 0; i < budget.expenses.variable.length; i++) {
      const variableExpense = budget.expenses.variable[i];
      expectedTally -= variableExpense.expected;
      for(let j = 0; j < variableExpense.currentItems.length; j++) {
        const currentItem = variableExpense.currentItems[0];
        actualTally -= currentItem.amount;
      }
    }

    return (
      <tr>
        <td>GRAND TOTAL</td>
        <td><span className="euros">{expectedTally.toFixed(2)}</span></td>
        <td><span className="euros">{actualTally.toFixed(2)}</span></td>
        <td>&nbsp;</td>
      </tr>
    )
  }

  const { income, expenses: { fixed: fixedExpenses, variable: variableExpenses } } = budget;
  const blankLine = <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>;

  return (
    <form key={`Form#${budget.name}`}>
      <table className="table budgetFullView">
        <thead>
        <tr>
          <th className="w-50" scope="col">Description</th>
          <th className="w-20" scope="col">Expected Amount</th>
          <th className="w-20" scope="col">Actual Amount</th>
          <th className="w-10" scope="col">&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>INCOME</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        { createIncomeRows(income) }
        { blankLine }
        <tr>
          <td>EXPENSES</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td>Fixed</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        { createFixedExpenses(fixedExpenses) }
        <tr>
          <td>Variable</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        { createVariableExpenses(variableExpenses) }
        { blankLine }
        { createTotals(budget) }
        </tbody>
      </table>
    </form>
  );
};

BudgetFullView.propTypes = {
  budget: PropTypes.object,
  incomeItemUpdated: PropTypes.func.isRequired,
  fixedExpenseItemUpdated: PropTypes.func.isRequired,
  variableExpenseItemUpdated: PropTypes.func.isRequired,
  deleteIncomeItem: PropTypes.func.isRequired,
  deleteFixedExpense: PropTypes.func.isRequired,
  deleteVariableExpense: PropTypes.func.isRequired,
  manageVariableExpense: PropTypes.func.isRequired
};
