import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Chart} from 'chart.js';
import {cloneDeep, first, groupBy, isEqual, last, reduce, sumBy} from 'lodash';
import moment from 'moment';
import {calculateRegression} from '../../utils/linearRegressionUtils';
import {calcDateRange} from '../../utils/dateRangeUtils';

export class VariableExpenseProjection extends Component {

  constructor(props) {
    super(props);
    this.state = {
      expense: cloneDeep(props.expense)
    }
  }

  componentDidMount() {
    const {expense, budgetDate} = this.props;
    const {dates, data, optimalData, regressionData} = this.getDatesAndDataForExpense(expense, budgetDate);
    let ctx = document.getElementById('chart').getContext('2d');
    let chart = new Chart(ctx, {
      // The type of chart we want to create
      type: 'line',

      // The data for our dataset
      data: {
        labels: dates,
        datasets: [
          this.createActualExpenseDataset(data, expense.name),
          this.createOptimalDataset(optimalData),
          this.createLinearRegressionDataset(regressionData)
        ]
      },

      // Configuration options go here
      options: {
        bezierCurve: false
      }
    });
    this.setState({chart});
  }

  getDatesAndDataForExpense(expense, budgetDate) {
    let expensesPerDay = groupBy(expense.currentItems, 'date');
    let dates = Object.keys(expensesPerDay)
      .map(date => moment(date, "YYYY/MM/DD"))
      .sort((a, b) => a.diff(b));


    let startDate = moment(budgetDate, "YYYY/MM/DD").startOf('month')
    if(dates.length > 0) {
      startDate = moment(first(dates)).subtract(1, 'days');
    }

    let endDate = moment(budgetDate, "YYYY/MM/DD").endOf('month');
    let range = calcDateRange(startDate, endDate);


    let regressionStartDate = moment(first(dates));
    let regressionLastDate = moment(last(dates));
    let today = moment().startOf('day');
    if(today <= endDate && regressionLastDate < today) {
      regressionLastDate = moment(today)
    }
    let regressionRange = calcDateRange(regressionStartDate, regressionLastDate);
    let regressionDates = regressionRange.map(d => d.format("YYYY/MM/DD"));

    let itemsPerDate = regressionDates.map(date => sumBy(expensesPerDay[date] || [], 'amount'));

    let data = reduce(itemsPerDate, (curVal, item) => {
      let prev = last(curVal);
      curVal.push(prev - item);
      return curVal;
    }, [expense.expected]);

    dates = range.map(d => d.format("YYYY/MM/DD"));

    let delta = expense.expected / (dates.length - 1);
    let optimalData = dates
      .map((date, index) => {
        return expense.expected - (index * delta)
      });

    let regressionData = calculateRegression(data, dates.length);

    return {dates, data, optimalData, regressionData}
  }

  componentDidUpdate() {
    const {expense, budgetDate} = this.props;
    const {expense: prevExpense} = this.state;
    if (!isEqual(prevExpense, expense)) {
      const {chart} = this.state;
      const {dates, data, optimalData, regressionData} = this.getDatesAndDataForExpense(expense, budgetDate);
      chart.data.labels = dates;
      chart.data.datasets.splice(0, chart.data.datasets.length);
      chart.data.datasets.push(this.createActualExpenseDataset(data, expense.name));
      chart.data.datasets.push(this.createOptimalDataset(optimalData));
      chart.data.datasets.push(this.createLinearRegressionDataset(regressionData));
      chart.update();
      this.setState({expense: cloneDeep(expense)})
    }
  }

  render() {
    const {expense} = this.state;
    return (
      <>
        <h2>{expense.name} Projection</h2>
        <canvas id="chart"/>
      </>
    );
  }


  createActualExpenseDataset(data, name) {
    return {
      label: name,
      backgroundColor: 'rgba(90, 90, 90, 0.4)',
      borderColor: 'rgba(120, 120, 120, 0.4)',
      data: data,
      lineTension: 0,
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "gray",
      pointHoverBorderColor: "black",
      pointHoverBorderWidth: 2
    }
  }

  createOptimalDataset(data) {
    return {
      label: "Optimal Line",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(225,0,0,0.4)",
      borderColor: "red", // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "red",
      pointHoverBorderColor: "black",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: data
    }
  }

  createLinearRegressionDataset(data) {
    return {
      label: "Linear Regression Projection",
      fill: false,
      lineTension: 0.1,
      backgroundColor: "rgba(0,255,0,0.4)",
      borderColor: "green", // The main line color
      borderCapStyle: 'square',
      borderDash: [], // try [5, 15] for instance
      borderDashOffset: 0.0,
      borderJoinStyle: 'miter',
      pointBorderColor: "black",
      pointBackgroundColor: "white",
      pointBorderWidth: 1,
      pointHoverRadius: 8,
      pointHoverBackgroundColor: "green",
      pointHoverBorderColor: "black",
      pointHoverBorderWidth: 2,
      pointRadius: 4,
      pointHitRadius: 10,
      data: data
    }
  }
}


VariableExpenseProjection.propTypes = {
  expense: PropTypes.object.isRequired,
  budgetDate: PropTypes.string
};
