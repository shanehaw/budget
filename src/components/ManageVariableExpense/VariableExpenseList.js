import React from 'react';
import PropTypes from 'prop-types';
import AutosizeInput from 'react-input-autosize';

export const VariableExpenseList = ({items, expenseItemUpdated, deleteExpenseItem}) => {
  function createExpenseItems(items) {
    const inputStyle = {border: 'none'};
    return items.map(i =>
      <tr key={i.id}>
        <td>
          <span>
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='text'
              onChange={(e) => expenseItemUpdated(e, i.id, "date")}
              value={i.date} />
          </span>
        </td>
        <td>
          <span>
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='text'
              onChange={(e) => expenseItemUpdated(e, i.id, "name")}
              value={i.name} />
          </span>
        </td>
        <td>
          <span className="euros">
            <AutosizeInput
              inputStyle={inputStyle}
              minWidth={5}
              type='number'
              extraWidth={0}
              onChange={(e) => expenseItemUpdated(e, i.id, "amount")}
              value={i.amount} />
          </span>
        </td>
        <td><button type="button" onClick={(e) => deleteExpenseItem(e, i.id)} className="btn btn-danger float-right">Delete</button></td>
      </tr>
    );
  }

  return (
    <table className="table">
      <thead>
      <tr>
        <th className="w-20" scope="col">Date</th>
        <th className="w-50" scope="col">Description</th>
        <th className="w-20" scope="col">Amount</th>
        <th className="w-10" scope="col">&nbsp;</th>
      </tr>
      </thead>
      <tbody>
      { createExpenseItems(items) }
      </tbody>
    </table>
  );
};

VariableExpenseList.propTypes = {
  items: PropTypes.array,
  expenseItemUpdated: PropTypes.func.isRequired,
  deleteExpenseItem: PropTypes.func.isRequired
};
