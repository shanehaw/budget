import React from 'react';
import PropTypes from 'prop-types';

export const VariableExpenseControlPanel = ({addExpenseItem, saveBudget, saveBudgetDisabled}) => {
  return (
    <div className="mb-2">
      <button type="button" className="btn btn-primary" onClick={addExpenseItem}>Add Expense Item</button>
      <button type="button" disabled={saveBudgetDisabled} className="btn btn-success float-right" onClick={saveBudget}>Save Changes</button>
    </div>
  );
};

VariableExpenseControlPanel.propTypes = {
  addExpenseItem: PropTypes.func.isRequired,
  saveBudget: PropTypes.func.isRequired,
  saveBudgetDisabled: PropTypes.bool.isRequired
};
