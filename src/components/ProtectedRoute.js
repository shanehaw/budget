import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as profileActions from '../actions/profileActions';
import { cloneDeep } from 'lodash';

class ProtectedRoute extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      profileId: props.profileId,
      budgetKey: props.budgetKey,
      budget: props.selectedBudget
    }
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(nextProps.profileId !== prevState.profileId
      || (typeof nextProps.budgetKey !== 'undefined' && nextProps.budgetKey !== prevState.budgetKey)
      || (typeof nextProps.selectedBudget !== 'undefined' && typeof prevState.budget === 'undefined')
      || (typeof nextProps.selectedBudget !== 'undefined' && nextProps.selectedBudget.key !== prevState.budget.key)) {

      tryLoadBudgetsForProfile(nextProps);

      return {
        profileId: nextProps.profileId,
        budgetKey: nextProps.budgetKey,
        budget: cloneDeep(nextProps.selectedBudget),
      };
    }
    return null;
  }

  componentDidMount() {
    tryLoadBudgetsForProfile(this.props);
  }

  render() {
    const { component: Component, ...rest } = this.props.ownProps;
    //const { profileId, budgetKey, selectedBudget, accessAllowed, isProfileLoading } = this.props;
    const { selectedBudget, accessAllowed, isProfileLoading } = this.props;
    const protectedRouteProps = {
      accessAllowed,
      selectedBudget,
      isProfileLoading
    };
    return(
      <Route
        {...rest}
        render={(props) => {
          return (
            <div>
              {/*<span>Debug info: {`profileId = "${profileId}" | budgetKey = "${budgetKey}" | accessAllowed = "${accessAllowed}" | isProfileLoading = "${isProfileLoading}" | selectedBudget = `}<pre>{`${JSON.stringify(selectedBudget, null, 2)}`}</pre></span>*/}
              <Component {...props} protectedRouteProps={protectedRouteProps}/>
            </div>
          );
        }}
      />
    );

  }
}

ProtectedRoute.propTypes = {
  profileId: PropTypes.string,
  budgetKey: PropTypes.string,
  selectedBudget: PropTypes.object,
  accessAllowed: PropTypes.bool,
  isProfileLoading: PropTypes.bool,
  ownProps: PropTypes.object
};

function tryLoadBudgetsForProfile(props) {
  const {profileId, selectedBudget} = props;
  if(typeof selectedBudget === 'undefined') {
    props.profileActions.getProfileBudgets(profileId);
  }
}

function findProfileBudget(budgets, profileId, budgetKey) {
  let selectedBudget = undefined;
  for (let i = 0; i < budgets.length; i++) {
    const budget = budgets[i];
    if (budget.userid == profileId && budget.key == budgetKey) {
      selectedBudget = cloneDeep(budget);
    }
  }
  return selectedBudget;
}

function mapStateToProps(state, ownProps) {
  const { id: profileId, budgetKey } = ownProps.computedMatch.params;
  const selectedBudget = findProfileBudget(state.profile.budgets, profileId, budgetKey);
  return {
    ownProps,
    profileId,
    budgetKey,
    selectedBudget,
    accessAllowed: state.profile.accessAllowed,
    isProfileLoading: state.profile.isLoading
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProtectedRoute);
