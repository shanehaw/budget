  import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as profileActions from '../actions/profileActions';
import {BudgetFullView} from './Budget/BudgetFullView';
import BudgetControlPanel from './Budget/BudgetControlPanel';
import {Link} from 'react-router-dom';
import {cloneDeep, isEqual} from 'lodash'
import {ContinueDialog} from './common/ContinueDialog';
import toastr from "toastr";

class BudgetPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      budgetIsFound: typeof this.props.selectedBudget !== 'undefined',
      budget: cloneDeep(props.selectedBudget),
      showContinue: false,
      idToGoTo: undefined
    };
    this.incomeItemUpdated = this.incomeItemUpdated.bind(this);
    this.fixedExpenseItemUpdated = this.fixedExpenseItemUpdated.bind(this);
    this.variableExpenseItemUpdated = this.variableExpenseItemUpdated.bind(this);
    this.addIncomeItem = this.addIncomeItem.bind(this);
    this.addFixedExpense = this.addFixedExpense.bind(this);
    this.addVariableExpenses = this.addVariableExpenses.bind(this);
    this.deleteIncomeItem = this.deleteIncomeItem.bind(this);
    this.deleteFixedExpense = this.deleteFixedExpense.bind(this);
    this.deleteVariableExpense = this.deleteVariableExpense.bind(this);
    this.manageVariableExpense = this.manageVariableExpense.bind(this);
    this.saveBudget = this.saveBudget.bind(this);
    this.onCancelMoveAway = this.onCancelMoveAway.bind(this);
    this.onContinueMoveAway = this.onContinueMoveAway.bind(this);
    this.hideDialog = this.hideDialog.bind(this);
    this.saveAndContinue = this.saveAndContinue.bind(this);
    this.returnToBudgets = this.returnToBudgets.bind(this);
    this.doBankReconciliation = this.doBankReconciliation.bind(this);
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if(typeof nextProps.selectedBudget !== typeof prevState.budget) {
      return {
        budgetIsFound: typeof nextProps.selectedBudget !== 'undefined',
        budget: cloneDeep(nextProps.selectedBudget)
      }
    }
    return null
  }

  getNotFoundContent() {
    const {userId} = this.props;
    const link = userId === 0 ? `/` : `/profile/${userId}`;
    const text = userId === 0 ? 'Home' : 'your profile';
    return <p>Either this budget does not exist, or you do not have access to it. Please <Link to={link}>click here</Link> to return to {text}.</p>;
  }

  saveBudget(event) {
    event.preventDefault();
    const { budget } = this.state;
    const { userId } = this.props;
    this.props.profileActions.saveBudget(userId, budget)
      .then(() => toastr.success("Successfully saved budget!"))
      .catch((err) => toastr.error(`An error occurred saving your budget: ${err}`))
      .then(() => this.props.profileActions.getProfileBudgets(userId));
  }

  returnToBudgets(event) {
    event.preventDefault();
    this.routeToProfileBudgets();
  }

  doBankReconciliation(event) {
    event.preventDefault();
    const { userId } = this.props;
    const { budget } = this.state;
    this.props.history.push(`/profile/${userId}/budget/${budget.key}/bank-reconciliation`)
  }

  routeToProfileBudgets() {
    const { userId } = this.props
    this.props.profileActions.getProfileBudgets(userId);
    this.props.history.push(`/profile/${userId}`);
  }

  incomeItemUpdated(event, name, attribute) {
    const budget = this.state.budget;
    for(let i = 0; i < budget.income.length; i++) {
      const incomeItem = budget.income[i];
      if(incomeItem.name === name) {
        if(attribute !== 'name') {
          incomeItem[attribute] = parseFloat(event.target.value);
        } else {
          incomeItem[attribute] = event.target.value;
        }

        this.setState({ budget });
        break;
      }
    }
  }

  fixedExpenseItemUpdated(event, name, attribute) {
    const budget = this.state.budget;
    for(let i = 0; i < budget.expenses.fixed.length; i++) {
      const fixedExpense = budget.expenses.fixed[i];
      if(fixedExpense.name === name) {
        if(attribute !== 'name') {
          fixedExpense[attribute] = parseFloat(event.target.value);
        } else {
          fixedExpense[attribute] = event.target.value;
        }
        this.setState({
          budget
        });
        break;
      }
    }
  }

  variableExpenseItemUpdated(event, name, attribute) {
    const budget = this.state.budget;
    for(let i = 0; i < budget.expenses.variable.length; i++) {
      const variableExpense = budget.expenses.variable[i];
      if(variableExpense.name === name) {
        if(attribute !== 'name') {
          variableExpense[attribute] = parseFloat(event.target.value);
        } else {
          variableExpense[attribute] = event.target.value;
        }
        this.setState({
          budget
        });
        break;
      }
    }
  }

  addIncomeItem() {
    const budget = this.state.budget;
    let currentMaxId = 0;
    for(let i = 0; i < budget.income.length; i++) {
      const incomeItem = budget.income[i];
      if(incomeItem.id > currentMaxId) {
        currentMaxId = incomeItem.id;
      }
    }
    const newIncomeItem = {
      id: currentMaxId + 1,
      name: 'New Income Item',
      expected: 0,
      actual: 0
    };
    budget.income.push(newIncomeItem);
    this.setState({
      budget
    });
  }

  addFixedExpense() {
    const budget = this.state.budget;
    let currentMaxId = 0;
    for(let i = 0; i < budget.expenses.fixed.length; i++) {
      const fixedExpense = budget.expenses.fixed[i];
      if(fixedExpense.id > currentMaxId) {
        currentMaxId = fixedExpense.id;
      }
    }
    const newFixedExpense = {
      id: currentMaxId + 1,
      name: 'New Expense',
      expected: 0,
      actual: 0
    };
    budget.expenses.fixed.push(newFixedExpense);
    this.setState({
      budget
    });
  }

  addVariableExpenses() {
    const budget = this.state.budget;
    let currentMaxId = 0;
    for(let i = 0; i < budget.expenses.variable.length; i++) {
      const variableExpense = budget.expenses.variable[i];
      if(variableExpense.id > currentMaxId) {
        currentMaxId = variableExpense.id;
      }
    }
    const newVariableExpense = {
      id: currentMaxId + 1,
      name: 'New Expense',
      expected: 0,
      currentItems: []
    };
    budget.expenses.variable.push(newVariableExpense);
    this.setState({
      budget
    });
  }

  deleteIncomeItem(id) {
    const budget = this.state.budget;
    for(let i = budget.income.length - 1; i >= 0; i--) {
      const item = budget.income[i];
      if(item.id === id) {
        budget.income.splice(i, 1);
        break;
      }
    }
    this.setState({
      budget
    });
  }

  deleteFixedExpense(id) {
    const budget = this.state.budget;
    for(let i = budget.expenses.fixed.length - 1; i >= 0; i--) {
      const item = budget.expenses.fixed[i];
      if(item.id === id) {
        budget.expenses.fixed.splice(i, 1);
        break;
      }
    }
    this.setState({
      budget
    });
  }

  deleteVariableExpense(id) {
    const budget = this.state.budget;
    for(let i = budget.expenses.variable.length - 1; i >= 0; i--) {
      const item = budget.expenses.variable[i];
      if(item.id === id) {
        budget.expenses.variable.splice(i, 1);
        break;
      }
    }
    this.setState({
      budget
    });
  }

  manageVariableExpense(id) {
    const { selectedBudget } = this.props;
    const { budget } = this.state;
    if(isEqual(selectedBudget, budget)) {
      this.moveToVariableExpenseManagingPage(id);
    } else {
      this.setState({showContinue: true, idToGoTo: id});
    }
  }

  onCancelMoveAway(e) {
    e.preventDefault();
    this.setState({showModal: false, variableExpenseId: -1});
  }

  onContinueMoveAway(e) {
    e.preventDefault();
    const { variableExpenseId } = this.state;
    this.setState({showModal: false, variableExpenseId: -1});
    this.moveToVariableExpenseManagingPage(variableExpenseId);
  }

  moveToVariableExpenseManagingPage(id) {
    const { userId, selectedBudget } = this.props;
    this.props.history.push(`/profile/${userId}/budget/${selectedBudget.key}/variable-expense/${id}`);
  }

  hideDialog() {
    this.setState({idToGoTo: undefined, showContinue: false});
  }

  saveAndContinue(idToGoTo) {
    const { budget } = this.state;
    const { userId } = this.props;
    this.props.profileActions.saveBudget(userId, budget)
      .then(() => {
        this.props.profileActions.getProfileBudgets(userId)
          .then(() => this.moveToVariableExpenseManagingPage(idToGoTo))
      })
      .catch((err) => toastr.error(`An error occurred saving your budget: ${err}`))
  }

  getBudgetFoundContent(budget, showContinue, idToGoTo) {
      return (
        <>
          <ContinueDialog
            show={showContinue}
            onDiscardAndContinue={() => this.moveToVariableExpenseManagingPage(idToGoTo)}
            onSaveAndContinue={() => this.saveAndContinue(idToGoTo)}
            onCancel={this.hideDialog}
            question="You have unsaved changes. What would you like to do?"/>
          <BudgetControlPanel
            addIncomeItem={this.addIncomeItem}
            addFixedExpense={this.addFixedExpense}
            addVariableExpenses={this.addVariableExpenses}
            saveBudget={this.saveBudget}
            returnToBudgets={this.returnToBudgets}
            doBankReconciliation={this.doBankReconciliation}/>
          <BudgetFullView
            budget={budget}
            incomeItemUpdated={this.incomeItemUpdated}
            fixedExpenseItemUpdated={this.fixedExpenseItemUpdated}
            variableExpenseItemUpdated={this.variableExpenseItemUpdated}
            deleteIncomeItem={this.deleteIncomeItem}
            deleteFixedExpense={this.deleteFixedExpense}
            deleteVariableExpense={this.deleteVariableExpense}
            manageVariableExpense={this.manageVariableExpense} />
        </>
      );
  }

  render() {
    const { accessAllowed, isProfileLoading } = this.props;
    const { budgetIsFound, budget, showContinue, idToGoTo } = this.state;

    let content = undefined;
    let heading = undefined;

    if (isProfileLoading) {
      heading = <h1>Profile Budget Page</h1>;
      content = <span>Loading...</span>;
    } else {

      heading = accessAllowed && budgetIsFound
        ? (<div><h1>{budget.name}</h1><h5>{budget.date}</h5></div>)
        : <h1>Unknown Budget</h1>;

      content = accessAllowed && budgetIsFound
        ? this.getBudgetFoundContent(budget, showContinue, idToGoTo)
        : this.getNotFoundContent();
    }

    return (
      <div>
        <div className="jumbotron text-center">
          {heading}
        </div>
        {content}
      </div>
    );
  }


}

BudgetPage.propTypes = {
  isProfileLoading: PropTypes.bool.isRequired,
  accessAllowed: PropTypes.bool,
  selectedBudget: PropTypes.object,
  userId: PropTypes.number,
  history: PropTypes.object.isRequired,
  profileActions: PropTypes.object.isRequired
};

function mapStateToProps(state, ownProps) {
  const { selectedBudget, accessAllowed, isProfileLoading } = ownProps.protectedRouteProps;
  return {
    isProfileLoading,
    accessAllowed,
    selectedBudget,
    userId: state.auth.user.userid
  };
}

function mapDispatchToProps(dispatch) {
  return {
    profileActions: bindActionCreators(profileActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BudgetPage);
