import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as profileActions from '../actions/profileActions';
import BudgetList from './ProfileBudgets/BudgetList';
import SubmitButton from './common/SubmitButton';
import { find, cloneDeep } from 'lodash';
import moment from 'moment';

class ProfilePage extends React.Component {
  constructor(props) {
    super(props);

    this.loadBudget = this.loadBudget.bind(this);
    this.createNewBudget = this.createNewBudget.bind(this);
    this.deleteBudget = this.deleteBudget.bind(this);
    this.duplicateBudgetToNextMonth = this.duplicateBudgetToNextMonth.bind(this);
  }

  loadBudget(event, key) {
    event.preventDefault();
    const { profileId } = this.props;
    this.props.history.push(`${profileId}/budget/${key}`);
  }

  createNewBudget(event) {
    event.preventDefault();
    const { profileId } = this.props;
    this.props.history.push(`${profileId}/create_budget`);
  }

  deleteBudget(event, key) {
    event.preventDefault();
    const { profileId } = this.props;
    this.props.actions.deleteBudget(profileId, key)
      .then(() => this.props.actions.getProfileBudgets(profileId))
      .catch((err) => alert(err));
  }

  duplicateBudgetToNextMonth(event, key) {
    event.preventDefault();
    const { budgets, profileId } = this.props;
    const budget = find(budgets, { key });
    let newBudget = cloneDeep(budget);
    let budgetDate = moment(budget.date, "YYYY/MM/DD").add(1, 'month');
    newBudget.date = budgetDate.format("YYYY/MM/DD");
    (newBudget.income || []).forEach(income => income.actual = 0);
    (newBudget.expenses.fixed || []).forEach(fe => fe.actual = 0);
    (newBudget.expenses.variable || []).forEach(ve => ve.currentItems = []);
    newBudget.reconciliation = {};

    this.props.actions.createNewBudget(profileId, newBudget.name, newBudget.date)
      .then(budget => {
        newBudget.key = budget.key;
        this.props.actions.saveBudget(profileId, newBudget)
          .then(() => this.props.actions.getProfileBudgets(profileId));
      });
  }

  render() {
    const {isProfileLoading, accessAllowed} = this.props;

    let content = undefined;
    if (isProfileLoading) {
      content = <span>Loading...</span>
    } else {
      if (!accessAllowed) {
        content = this.createAccessDeniedContent();
      } else {
        content = this.createBudgetList()
      }
    }

    return (
      <div>
        <div className="jumbotron text-center">
          <h1>Profile Budgets</h1>
        </div>
        <div className="container">
          {content}
        </div>
      </div>
    );
  }

  createBudgetList() {
    const {budgets} = this.props;
    return (
      <div>
        <BudgetList
          budgets={budgets}
          loadBudget={this.loadBudget}
          deleteBudget={this.deleteBudget}
          duplicateBudgetToNextMonth={this.duplicateBudgetToNextMonth} />
        <hr />
        <SubmitButton
          onClick={this.createNewBudget}
          text="Create New"
          disabled={false} />
      </div>
    );
  }

  createAccessDeniedContent() {
    const {profileId} = this.props;
    const link = profileId === 0 ? `/` : `/profile/${profileId}`;
    const text = profileId === 0 ? 'Home' : 'your profile';
    return <p>Either this profile does not exist, or you do not have access to it. Please <Link to={link}>click here</Link> to return to {text}.</p>;
  }
}

ProfilePage.propTypes = {
  isProfileLoading: PropTypes.bool.isRequired,
  accessAllowed: PropTypes.bool.isRequired,
  history: PropTypes.object.isRequired,
  budgets: PropTypes.arrayOf(PropTypes.object),
  actions: PropTypes.object,
  profileId: PropTypes.number
};

function mapStateToProps(state, ownProps) {
  const { accessAllowed, isProfileLoading } = ownProps.protectedRouteProps;
  return {
    accessAllowed,
    isProfileLoading,
    budgets: state.profile.budgets,
    profileId: state.auth.user.userid
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(profileActions, dispatch)
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfilePage);
