import React from 'react';
import PropTypes from 'prop-types';
import TextInput from '../common/TextInput';
import SubmitButton from '../common/SubmitButton';
import ServersideFormError from '../common/ServersideFormError';
import './RegistrationForm.css';

const RegistrationForm = ({ registeringUser, errors, registerUser, onRegistrationValueChange, isAttemptingRegistration }) => {
  const { usernameError, password1Error, password2Error, serversideError } = errors;
  let hasError = serversideError && serversideError.length > 0;
  return (
    <form className="form-horizontal">
      <div className="container">
        <div className="row">
          <TextInput
            label="Username:"
            type="text"
            id="registrationFormUserName"
            name="username"
            value={registeringUser.username}
            onChange={onRegistrationValueChange}
            error={usernameError} />
          <TextInput
            label="Password:"
            type="password"
            id="registrationFormPassword"
            name="password"
            value={registeringUser.password}
            onChange={onRegistrationValueChange}
            error={password1Error}/>
          <TextInput
            label="Confirmation Password:"
            type="password"
            id="registrationFormPassword2"
            name="password2"
            value={registeringUser.password2}
            onChange={onRegistrationValueChange}
            error={password2Error} />
          {hasError && <ServersideFormError error={serversideError} />}
          <SubmitButton
            disabled={isAttemptingRegistration}
            onClick={registerUser}
            text="Register"
            bootstrapClassName="offset-sm-3 col-sm-6" />
        </div>
      </div>
    </form>
  );
};

RegistrationForm.propTypes = {
  registeringUser: PropTypes.object.isRequired,
  errors: PropTypes.object,
  registerUser: PropTypes.func.isRequired,
  onRegistrationValueChange: PropTypes.func.isRequired,
  isAttemptingRegistration: PropTypes.bool.isRequired
};

export default RegistrationForm;

