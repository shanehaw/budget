// Set up your root reducer here...
import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import profile from './profileReducer';
import auth from './authReducer';
import registration from './registrationReducer';


const rootReducer = history => combineReducers({
  router: connectRouter(history),
  profile,
  auth,
  registration,
});

export default rootReducer;
