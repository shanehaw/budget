import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function registrationReducer(state = initialState.registration, action) {
  switch(action.type) {

    case types.REGISTRATION_REQUEST:
      return Object.assign({}, state, {
        isRegistering: action.isRegistering,
        registeringUser: Object.assign({}, action.registration)
      });
    case types.REGISTRATION_SUCCESS:
      return Object.assign({}, state, {
        isRegistering: false,
        registeringUser: {
          username:'',
          password:'',
          password2:''
        }
      });
    case types.REGISTRATION_FAILURE:
      return Object.assign({}, state, {
        isRegistering: false,
        registeringUser: {
          username:'',
          password:'',
          password2:''
        }
      });
    default:
      return state;
  }
}
