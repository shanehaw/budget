import * as types from '../actions/actionTypes';
import intialState from './initialState';

export default function authReducer(state = intialState.auth, action) {
  switch(action.type) {
    case types.LOGIN_REQUEST:
      return Object.assign({}, state, {
        isFetching: action.isFetching,
        isAuthenticated: action.isAuthenticated,
        user: Object.assign({}, action.credentials)
      });
    case types.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isFetching: action.isFetching,
        isAuthenticated: action.isAuthenticated,
        user: Object.assign({}, state.user, {
          userid: action.userid
        })
      });
    case types.LOGIN_FAILURE:
      return Object.assign({}, state, {
        isFetching: action.isFetching,
        isAuthenticated: action.isAuthenticated
      });
    case types.LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        user: {
          userid: 0,
          username: '',
          password: ''
        },
        isFetching: action.isFetching,
        isAuthenticated: action.isAuthenticated
      });
    case types.REGISTRATION_SUCCESS:
      return Object.assign({}, state, {
        user: {
          username: action.registration.username,
          password: action.registration.password,
          userid: action.userid
        },
        isAuthenticated: true
      });
    default:
      return state;
  }
}
