import * as types from '../actions/actionTypes';
import intialState from './initialState';

export default function authReducer(state = intialState.profile, action) {
  switch(action.type) {
    case types.PROFILE_BUDGETS_REQUEST:
      return Object.assign({}, state, {
          isLoading: true,
          accessAllowed: false
        });
    case types.PROFILE_BUDGETS_SUCCESS:
      return Object.assign({}, state, {
        isLoading: false,
        accessAllowed: true,
        budgets: action.budgets
      });
    case types.PROFILE_BUDGETS_FAILURE:
    case types.PROFILE_BUDGETS_FORBIDDEN:
      return Object.assign({}, state, {
        isLoading: false,
        accessAllowed: false,
      });
    case types.PROFILE_BUDGET_SELECT:
      return Object.assign({}, state, {
        selectedBudgetKey: action.budgetKey
      });

    case types.LOGOUT_SUCCESS:
      return Object.assign({}, state, {
        budgets: []
      });

    default:
      return state;
  }
}
