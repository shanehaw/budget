const jwtDecode = require('jwt-decode');

function assertAlive (decoded) {
  const now = Date.now().valueOf() / 1000
  if (typeof decoded.exp !== 'undefined' && decoded.exp < now) {
    throw new Error(`token expired: ${JSON.stringify(decoded)}`)
  }
  if (typeof decoded.nbf !== 'undefined' && decoded.nbf > now) {
    throw new Error(`token expired: ${JSON.stringify(decoded)}`)
  }
}

function getInitialUser() {
  try {
    const encodedToken = localStorage.getItem('id_token');
    if(encodedToken) {
      const token = jwtDecode(encodedToken);
      if(token) {
        assertAlive(token);        
        return {
          isAuthenticated: true,
          userid: token.id,
          username: token.username
        };
      }
    }
  } catch (error) {
    console.log(error); /* eslint-disable-line no-console */
  }
  return {
    isAuthenticated: false,
    userid: 0,
    username: ''
  };
}

const initialUser = getInitialUser();

export default {
  profile: {
    isLoading: false,
    accessAllowed: false,
    selectedBudgetKey: '',
    budgets: []
  },
  auth: {
    isFetching: false,
    isAuthenticated: initialUser.isAuthenticated,
    user: {
      userid: initialUser.userid,
      username: initialUser.username,
      password: ''
    }
  },
  registration: {
    isRegistering: false,
    registeringUser: {
      username: '',
      password: '',
      password2: ''
    }
  }
};
